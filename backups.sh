#!/bin/bash

target_location="/root/shared/backups/"

mkdir -p ${target_location}/daily/wordpress/

rsync -av --delete /root/wordpress/ ${target_location}/daily/wordpress/
rsync -av --delete /var/lib/docker/volumes/wordpress_* ${target_location}/daily/

# daily backups
tar -cvzf ${target_location}/daily_$(date +%Y%m%d).tar.gz  ${target_location}/daily/

# weekly backups
day=`date +%w`
day_of_week=0
# only runs of dsya of the week
if [ $day = $day_of_week ]
then 
tar -cvzf ${target_location}/weekly_$(date +%Y%m%d).tar.gz  ${target_location}/daily/
else
echo "no weekly backup"
fi

# monthly backups
day=`date +%d`
day_of_month=1
# only runs on day of the month
if [ $day = $day_of_month ]
then 
tar -cvzf ${target_location}/monthly_$(date +%Y%m%d).tar.gz  ${target_location}/daily/
else
echo "no monthly backup"
fi

# Rotation - keep last N files...
ls ${target_location}/daily_* -tr | head -n -7 | xargs --no-run-if-empty rm -rf
ls ${target_location}/weekly__* -tr | head -n -2 | xargs --no-run-if-empty rm -rf
ls ${target_location}/monthly__* -tr | head -n -2 | xargs --no-run-if-empty rm -rf
